# HIANIC Designed Pedestrians-Vehicle Interaction Experiment - Nantes

This repository containes the spatio-temporal data recorded during a set of designed pedestrians-vehicle interaction scenarios.

The data is recorded on 15/10/2020 at the parking lot of the [LS2N](https://www.ls2n.fr) lab on the [École Centrale de Nantes](https://www.ec-nantes.fr) campus


<img src= images/top_view_test_evn_0.jpg  width=500/>

The experiment invloved 17 volunteers of the lab members, students and professors:
- 2 female volunteers
- 15 male volunteers
- 1 male volunteer with reduced mobility (wheelchair)


The data is collected on board of a Renault Fluence vehicle with a driver, equipped with the following perception system:
- Four USB cameras UI-3240CP-C-HQ Rev.2 from iDS with lens Kowa, LM6HC, 6 mm, 1"
- One Velodyne VLP-16
- One IMU MTI-100-2A5G4 from Xsens
- One GPS EVK-M8T from U-Blox

<img src= images/test_vehicle_fluence_01.jpg  width=500/> | <img src= images/test_vehicle_fluence_05.jpg  width=500/>


## Workflow:

<img src= images/workflow.png  width=1000/>

## Project Folders:

### raw offile:
This folder containes the raw data recorded on board of the vehicle.
Each sub-folder containes the data of one recorded pedestrians-vehicle interaction (one scenario), with the following items:
- Ros bag of the raw recorded data with two topic: one for the pedestrian tracking and the other for the vehicle data.
- Two csv files containing the two ros bag topics.
- An image of the raw visulization of the trajectories in the XY-Plane.
- A gif for the progression of the trajectories in the XY-Plane across the recording time.

### fitltered offile:
This folder containes the csv files generated after the filtering and after removing the duplicates.

### annotations:
A file providing manual annotations of the experiment scenarios regarding the number of participants, the behaviour of the driver, ... etc.
