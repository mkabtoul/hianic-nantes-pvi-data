'''
This script saves each topic in a bagfile as a csv.

Accepts a filename as an optional argument. Operates on all bagfiles in current directory if no argument provided

Usage1 (for one bag file):
	python bag2csv.py filename.bag
Usage 2 (for all bag files in current directory):
	python bag2csv.py

Written by Nick Speal in May 2013 at McGill University's Aerospace Mechatronics Laboratory. Bugfixed by Marc Hanheide June 2016.
www.speal.ca

Supervised by Professor Inna Sharf, Professor Meyer Nahon

'''

import rosbag, sys, csv
import time
import string
import os #for file management make directory
import shutil #for file management, copy file
import pandas as pd

#verify correct input arguments: 1 or 2
if (len(sys.argv) > 2):
	print ("invalid number of arguments:   " + str(len(sys.argv)))
	print ("should be 2: 'bag2csv.py' and 'bagName'")
	print ("or just 1  : 'bag2csv.py'")
	sys.exit(1)
elif (len(sys.argv) == 2):
	listOfBagFiles = [sys.argv[1]]
	numberOfFiles = 1
	print ("reading only 1 bagfile: " + str(listOfBagFiles[0]))
elif (len(sys.argv) == 1):
	listOfBagFiles = [f for f in os.listdir(".") if f[-4:] == ".bag"]	#get list of only bag files in current dir.
	numberOfFiles = str(len(listOfBagFiles))
	print ("reading all " + numberOfFiles + " bagfiles in current directory: \n")
	for f in listOfBagFiles:
		print (f)
	print ("\n press ctrl+c in the next 10 seconds to cancel \n")
	time.sleep(1)
else:
	print ("bad argument(s): " + str(sys.argv))	#shouldnt really come up
	sys.exit(1)

count = 0
for bagFile in listOfBagFiles:
	count += 1
	print ("reading file " + str(count) + " of  " + str(numberOfFiles) + ": " + bagFile)
	#access bag
	bag = rosbag.Bag(bagFile)
	bagContents = bag.read_messages()
	bagName = bag.filename


	#create a new directory
	folder = string.rstrip(bagName, ".bag")
	try:	#else already exists
		os.makedirs(folder)
	except:
		pass
	shutil.copyfile(bagName, folder + '/' + bagName)

	start_secs = 0
	#get list of topics from the bag
	listOfTopics = []
	for topic, msg, t in bagContents:
		if topic not in listOfTopics:
			listOfTopics.append(topic)
	id_max = 0
	drop_flag = False
	for topicName in listOfTopics:
		if topicName == '/EEC_C200/icars_3d_pedestrian_tracker/tracked_pedestrians':
			for subtopic, msg, t in bag.read_messages(topicName):	# for each instant in time that has data for topicName
				#parse data from this instant, which is of the form of multiple lines of "Name: value\n"
				#	- put it in the form of a list of 2-element lists
				msgString = str(msg)
				msgList = string.split(msgString, '\n')
				instantaneousListOfData = []
				for nameValuePair in msgList:
					splitPair = string.split(nameValuePair, ':')
					for i in range(len(splitPair)):	#should be 0 to 1
						splitPair[i] = string.strip(splitPair[i])
					instantaneousListOfData.append(splitPair)
				#write the first row from the first element of each pair
				for pair in instantaneousListOfData:
					if pair[0] == 'id':
						id_max = max(int(id_max), int(pair[1]))
		if topicName == '/EEC_C200/position/ENU_Map_position':
			filename = folder + '/' + string.replace(topicName, '/', 'processed_') + '.csv'
			with open(filename, 'w+') as csvfile:
				filewriter = csv.writer(csvfile, delimiter = ',')
				firstIteration = True	#allows header row
				for subtopic, msg, t in bag.read_messages(topicName):	# for each instant in time that has data for topicName
					#parse data from this instant, which is of the form of multiple lines of "Name: value\n"
					#	- put it in the form of a list of 2-element lists
					msgString = str(msg)
					msgList = string.split(msgString, '\n')
					instantaneousListOfData = []
					for nameValuePair in msgList:
						splitPair = string.split(nameValuePair, ':')
						for i in range(len(splitPair)):	#should be 0 to 1
							splitPair[i] = string.strip(splitPair[i])
						instantaneousListOfData.append(splitPair)
					#write the first row from the first element of each pair
					if firstIteration:	# header
						headers = ["rosbagTimestamp"]	#first column header
						for pair in instantaneousListOfData:
							headers.append(pair[0])
							if pair[0] == 'secs':
								secs = float(pair[1])
							if pair[0] == 'nsecs':
								nsecs = float(pair[1])
						start_time = secs + 1e-9 * nsecs
						filewriter.writerow(headers)
						firstIteration = False
					# write the value from each pair to the file
					values = [str(t)]	#first column will have rosbag timestamp
					time_now = float(instantaneousListOfData[3][1]) + 1e-9 * float(instantaneousListOfData[4][1]) 
					if time_now < start_time:
						print('dropping one point, time incorrect')
						drop_flag = True
					adjusted_time = time_now - start_time
					for pair in instantaneousListOfData:
						if len(pair)>1:
							if pair[0] == 'secs':
								values.append(adjusted_time // 1) 
							else:
								if pair[0] == 'nsecs':
									values.append(1e+9 * (adjusted_time - adjusted_time // 1)) 
								else:
									if drop_flag == False:
										values.append(pair[1])
									else:
										drop_flag = False
					filewriter.writerow(values)
	ids_list = range(int(id_max) + 1)[1:]
	topicName = '/EEC_C200/icars_3d_pedestrian_tracker/tracked_pedestrians'
	filename = folder + '/' + string.replace(topicName, '/', 'processed_') + '_.csv'
	with open(filename, 'w+') as csvfile:
		filewriter = csv.writer(csvfile, delimiter = ',')
		firstIteration = True	#allows header row
		for ped_id in ids_list:
			ped_csv = pd.DataFrame()
			for topicName in listOfTopics:
				if topicName == '/EEC_C200/icars_3d_pedestrian_tracker/tracked_pedestrians':
					#Create a new CSV file for each topic
					for subtopic, msg, t in bag.read_messages(topicName):	# for each instant in time that has data for topicName
						#parse data from this instant, which is of the form of multiple lines of "Name: value\n"
						#	- put it in the form of a list of 2-element lists
						msgString = str(msg)
						msgList = string.split(msgString, '\n')
						instantaneousListOfData = []
						for nameValuePair in msgList:
							# print(nameValuePair)
							splitPair = string.split(nameValuePair, ':')
							for i in range(len(splitPair)):	#should be 0 to 1
								splitPair[i] = string.strip(splitPair[i])
							instantaneousListOfData.append(splitPair)
						constList = instantaneousListOfData[:6]
						subList = []
						# print(constList)
						time_now = float(instantaneousListOfData[3][1]) + 1e-9 * float(instantaneousListOfData[4][1]) 
						if time_now < start_time:
							print('dropping one point, time incorrect')
							drop_flag = True
						adjusted_time = time_now - start_time
						for pair in instantaneousListOfData:
							if pair[0] == 'id':
								if int(pair[1]) == ped_id:
								   id_index = instantaneousListOfData.index(['id', str(ped_id)])
								   ped_list = instantaneousListOfData[id_index:id_index+15]
								   subList = constList + ped_list
						if len(subList) > 1:
							if firstIteration:
								headers = ["rosbagTimestamp"]	#first column header
								for pair in subList:
									headers.append(pair[0])
								filewriter.writerow(headers)
								firstIteration = False
					#write the first row from the first element of each pair
							values = [str(t)]	#first column will have rosbag timestamp
							for pair in subList:
								if len(pair)>1:
									if pair[0] == 'secs':
										values.append(adjusted_time // 1) 
									else:
										if pair[0] == 'nsecs':
											values.append(1e+9 * (adjusted_time - adjusted_time // 1)) 
										else:
											if drop_flag == False:
												values.append(pair[1])
											else:
												drop_flag = False
							filewriter.writerow(values)
	bag.close()
print ("Done reading all " + str(numberOfFiles) + " bag files.")
