import math as m
import matplotlib as mplt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.patches import Rectangle
from matplotlib.animation import FuncAnimation
import os

plt.rcParams["figure.figsize"] = [3 * i for i in plt.rcParams["figure.figsize"]]

# Load Nantes data
local_dir = "../filtered offline/"

for filename in ['2020-10-15-11-31-09']:# os.listdir(local_dir):
   print(filename)
   # AV data
   df_car = pd.read_csv(local_dir + filename+ 
   #"/processed_EEC_C200processed_positionprocessed_ENU_Map_position.csv")
   "/clean_ENU_Map_position.csv")
  

   # remove lines before initialization completed
   df_car = df_car[(df_car.xGlobal != 0) | (df_car.yGlobal != 0)]
   # remove erroneous lines
   df_car = df_car[(((df_car.xGlobal - df_car.shift().xGlobal) < 0.5) &
                    ((df_car.yGlobal - df_car.shift().yGlobal) < 0.5)) &
                    (df_car.stamp != -1)]


   # get one row per 0.1s
   end = int(df_car[np.isnan(df_car.header)].secs.max())
   start = int(df_car[np.isnan(df_car.header)].secs.min())
   print(str(start)+"s - "+str(end)+"s")
   step = 0.1


   dataCar = pd.DataFrame(columns=df_car.columns)
   df_car["time"] = np.round(df_car.secs.astype(float)+ df_car.nsecs.astype(float) / 1000000000, 1)
   for i in np.arange(start, end, step):
       dataCar = dataCar.append(df_car[abs(df_car.time-round(i,1))<step/10.0].mean(axis=0), ignore_index=True)


   # Pedestrians data
   dataPedId = {}
   dataPed = {}

   df_peds = pd.read_csv(local_dir + filename +
  # "/processed_EEC_C200processed_icars_3d_pedestrian_trackerprocessed_tracked_pedestrians_.csv")
                         "/no_duplicate_clean_tracked_pedestrians_.csv")

   # remove erroneous lines
   df_peds = df_peds[(df_peds.stamp.isnull()) ]


   df_peds["time"] = np.round(df_peds.secs.astype(float) + df_peds.nsecs.astype(float) / 1000000000, 1)
   nbPed = df_peds.id.max()
   print(str(df_peds.id.nunique())+' ped. detected')
   
   print(df_peds.id.unique())
   for ped_id in df_peds.id.unique():
       idp = ped_id
       dataPedId[idp] = df_peds[(df_peds.id == ped_id) & (df_peds.type == 1)]
       dataPed[idp] = pd.DataFrame(columns=df_peds.columns)
       for i in np.arange(start, end, step):
            dataPed[idp] = dataPed[idp].append(dataPedId[idp][abs(dataPedId[idp].time-round(i,1))<step/10.0].mean(axis=0), ignore_index=True)


  
   # Prepare plot
   fig, ax = plt.subplots()
   plt.gca().set_xlim([dataCar.xGlobal.min() - 5, dataCar.xGlobal.max() + 5])
   plt.gca().set_ylim([dataCar.yGlobal.min() - 5, dataCar.yGlobal.max() + 5])
   plt.gca().set_aspect('equal', adjustable='box')
   
   # Draw car
   width = 4.6
   height = 1.8
   ts = ax.transData

   for index, row  in dataCar.iterrows():
      tr = mplt.transforms.Affine2D().rotate_deg_around(row['xGlobal'], row['yGlobal'], row['heading'] * (180.0 / 3.14))
      t = tr + ts
      rect1 = mplt.patches.Rectangle(xy=(row['xGlobal'] - width / 2, row['yGlobal'] - height / 2),
                                         width=width, height=height, linewidth=1, color='black', fill=False, transform=t)
      ax.add_patch(rect1)
            
   plt.scatter(dataCar.xGlobal, dataCar.yGlobal, s=100, marker='o', color='k')

   # Draw ped
   cmap = plt.get_cmap('gist_rainbow')
   colors = [cmap(i) for i in np.linspace(0, 1, df_peds.id.max()+1)]

   for n in df_peds.id.unique():
      mark = 'o'
      df_ped = df_peds[df_peds.id == n]
      #plt.scatter(df_ped['x'], df_ped['y'], c=colors[n], s=50, marker=mark, edgecolors='black')
      ax.plot(df_ped['x'], df_ped['y'], c=colors[n], linewidth=3)
   fig.savefig(filename+'.png')


'''

   # Draw car
   width = 4.6
   height = 1.8
   ts = ax.transData

   # Draw ped
   cmap = plt.get_cmap('gist_rainbow')
   colors = [cmap(i) for i in np.linspace(0, 1, nbPed+1)]

   def update(i):
       if not dataCar.iloc[i].empty:
         row = dataCar.iloc[i]
         tr = mplt.transforms.Affine2D().rotate_deg_around(row['xGlobal'], row['yGlobal'], row['heading'] * (180.0 / 3.14))
         t = tr + ts
         rect1 = mplt.patches.Rectangle(xy=(row['xGlobal'] - width / 2, row['yGlobal'] - height / 2),
                                      width=width, height=height, linewidth=1, color='black', fill=False,
                                      alpha=((1.1 * i) / dataCar.tail(1).index.item()) / 10.0, transform=t)
         ax.add_patch(rect1)
         plt.arrow(row['xGlobal'], row['yGlobal'],
                 row['lon_speed'] * m.cos(row['heading']) - row['lat_speed'] * m.sin(row['heading']),
                 row['lon_speed'] * m.sin(row['heading']) + row['lat_speed'] * m.cos(row['heading']))

         for n, data in dataPed.items():
            if not data.iloc[i].empty:
               mark = 'o'
               plt.scatter(data.iloc[i]['x'], data.iloc[i]['y'], c=colors[n], s=50, marker=mark, edgecolors='black')
               ax.annotate(data.iloc[i]['id'], (data.iloc[i]['x'], data.iloc[i]['y']))
   anim = FuncAnimation(fig, update, frames=len(dataCar.index), interval=1, repeat=False)
   plt.show()
   #anim.save(filename+'.gif', writer='imagemagick')
   fig.savefig(filename+'.png')
'''
