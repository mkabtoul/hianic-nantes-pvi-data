import math as m
import numpy as np
import pandas as pd
import os



# Load data
local_dir = "../filtered offline/"

for filename in os.listdir(local_dir):
   if(filename !="filter_snapshot_new_not_used"):
      print(filename)
       
      # Load filtered pedestrians data
      dataPedId = {}

      df_peds = pd.read_csv(local_dir + filename +"/clean_tracked_pedestrians_.csv")

      for ped_id in df_peds.id.unique():
       idp = ped_id
       dataPedId[idp] = df_peds[(df_peds.id == ped_id) & (df_peds.type == 1)]

      # Remove pedestrians duplicated (mean trajectory dist <10cm)
      dataPedByIdFilteredDup = {}
      toremove=[]
      
      for ped_id in dataPedId.keys():
         for ped_id_bis in dataPedId.keys():
            if(ped_id < ped_id_bis):
                diff = np.sqrt((dataPedId[ped_id].reset_index()["x"]-dataPedId[ped_id_bis].reset_index()["x"])**2 + (dataPedId[ped_id].reset_index()["y"]-dataPedId[ped_id_bis].reset_index()["y"])**2)
                if(diff.mean()<0.1):
                  print(str(ped_id)+" "+ str(ped_id_bis) +" " + str(diff.mean()))
                  toremove.append(ped_id_bis)
         dataPedByIdFilteredDup[ped_id] = dataPedId[ped_id]

      for rem_id in toremove:
         dataPedByIdFilteredDup.pop(rem_id, None)

      
      
      # Write clean data to csv 
      df_p = pd.concat(dataPedByIdFilteredDup.values(), ignore_index=True)
      df_p.to_csv(local_dir + filename + "/no_duplicate_clean_tracked_pedestrians_.csv", index=False) 
