import math as m
import numpy as np
import pandas as pd
import os
import csv

import matplotlib as mplt
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle



# Car data
def treatCarCSV(filename):
   df_car = pd.read_csv(local_dir + filename + "/processed_EEC_C200processed_positionprocessed_ENU_Map_position.csv")

   # remove rows before initialization completed
   df_car = df_car[(df_car.xGlobal != 0) | (df_car.yGlobal != 0)]
   
   # remove erroneous rows (stamp not null)
   df_car = df_car[(df_car.stamp.isnull())]
       
   # add time column   
   df_car["time"] = np.round(df_car.secs.astype(float)+ df_car.nsecs.astype(float) / 1000000000, 2)
   
   
   # remove rows with isolated AV positions at the begining
   errorTime = df_car[((abs(df_car.xGlobal - df_car.shift().xGlobal) > 0.5) |
                       (abs(df_car.yGlobal - df_car.shift().yGlobal) > 0.5))].time   
   if(not errorTime.empty):
     if(errorTime.iloc[0] < 5):
      print("erroneous car position at " +str(errorTime.iloc[0]))
      df_car = df_car[(df_car.time > errorTime.iloc[0])]
      
  
   # remove rows before car start to move
   nbSec_car = df_car.time.max()
   startTime = df_car.time.min()
   
   for s in np.arange(m.ceil(startTime), m.ceil(nbSec_car), 1):
      if ((
      abs(df_car[df_car.secs == s].iloc[0].xGlobal -df_car[df_car.secs == s+1].iloc[0].xGlobal)> 0.5) | 
      (abs(df_car[df_car.secs == s].iloc[0].yGlobal -df_car[df_car.secs == s+1].iloc[0].yGlobal) > 0.5)):
         break
               
   carStartMove = s
   df_car = df_car[(df_car.time > carStartMove)]   
   
      
   # remove rows after car stop to move
   stopDuration = 5 #to adapt to the scenario (from 1s to 15s)
   nbSec_car = df_car.time.max()
   startTime = df_car.time.min()
   
   for s in np.arange(m.ceil(startTime), m.ceil(nbSec_car), 1):
     if(not df_car[df_car.secs == s+stopDuration].empty):
      if ((
      abs(df_car[df_car.secs == s].iloc[0].xGlobal -df_car[df_car.secs == s+stopDuration].iloc[0].xGlobal)< 0.2) & 
      (abs(df_car[df_car.secs == s].iloc[0].yGlobal -df_car[df_car.secs == s+stopDuration].iloc[0].yGlobal)< 0.2)):
         break
   carStopMove = s-1
   df_car = df_car[(df_car.time < carStopMove)]    
   
   return df_car



# Pedestrians data
def treatPedCSV(filename, df_car):
   df_peds = pd.read_csv(local_dir + filename +
                         "/processed_EEC_C200processed_icars_3d_pedestrian_trackerprocessed_tracked_pedestrians_.csv")

   # remove erroneous rows (stamp not null)
   df_peds = df_peds[(df_peds.stamp.isnull())]
   
   # to check
   #print(df_peds[(df_peds.type != 1)])
   # remove erroneous rows (type different from 1)
   # df_peds = df_peds[(df_peds.type == 1)]
   
   # add time column  
   df_peds["time"] = np.round(df_peds.secs.astype(float) + df_peds.nsecs.astype(float) / 1000000000, 2)
      
   # check ped ids
   nbPed = df_peds['id'].nunique()
   min_id = df_peds.id.min()
   max_id = df_peds.id.max()
   print(str(nbPed)+' ped. detected (id from '+ str(min_id) + ' to ' + str(max_id)+')' )
   
   
   # remove rows before car start to move
   df_peds = df_peds[(df_peds.time >= df_car.time.min())]
   
   # remove rows after car stop to move
   df_peds = df_peds[(df_peds.time <= df_car.time.max())]
   
   return df_peds, nbPed, min_id, max_id
   



def filterPedTime(dataPedById):
   dataPedByIdFiltered = {}
   # Remove pedestrians that appear less than 3s
   time = 2 # to adapt to each scenario (from 2s to 3s)
   
   for ped_id in dataPedById.keys():
      ped = dataPedById[ped_id]  
      if (ped.time.max() - ped.time.min()>time):
         dataPedByIdFiltered[ped_id] = ped
   
   # check ped ids
   nbPed = len(dataPedByIdFiltered.keys())
   min_id = min(dataPedByIdFiltered.keys())
   max_id = max(dataPedByIdFiltered.keys())
   print('after time filter: '+str(nbPed)+' ped. detected (id from '+ str(min_id) + ' to ' + str(max_id)+')' )
   
   return dataPedByIdFiltered, nbPed, min_id, max_id






def filterPedDistance(dataPedByIdFiltered):
   dataPedByIdFilteredDist = {}
   distance = 2 # to adapt to each scenario (from 0m to 4m), 0 if there are standing ped
   
   # Remove pedestrians that move less than 3m
   for ped_id in dataPedByIdFiltered.keys():
      ped = dataPedByIdFiltered[ped_id]    
       
      if (((ped.x.max() - ped.x.min()) >distance) | ((ped.y.max() - ped.y.min()) >distance)):
         dataPedByIdFilteredDist[ped_id] = ped
   
   # check ped ids
   nbPed = len(dataPedByIdFilteredDist.keys())
   min_id = min(dataPedByIdFilteredDist.keys())
   max_id = max(dataPedByIdFilteredDist.keys())
   print('after dist filter: '+str(nbPed)+' ped. detected (id from '+ str(min_id) + ' to ' + str(max_id)+')' )
   
   return dataPedByIdFilteredDist, nbPed, min_id, max_id




def filterPedTooFar(dataPedByIdFiltered):
   dataPedByIdFilteredFar = {}
   # Remove pedestrians that move less than 3m
   for ped_id in dataPedByIdFiltered.keys():
      ped = dataPedByIdFiltered[ped_id]    
     
     # example custom filter for 2020-10-15-11-51-50
      if ((ped.y.max()>765) & (ped.x.max()<1183) & (ped.x.max()>1173)):
         dataPedByIdFilteredFar[ped_id] = ped
   
   # check ped ids
   nbPed = len(dataPedByIdFilteredFar.keys())
   min_id =min(dataPedByIdFilteredFar.keys())
   max_id = max(dataPedByIdFilteredFar.keys())
   print('after manual filter: '+str(nbPed)+' ped. detected (id from '+ str(min_id) + ' to ' + str(max_id)+')' )
   
   return dataPedByIdFilteredFar, nbPed, min_id, max_id



# Load data
local_dir = '/home/manon/Documents/HIANIC/Nantes/Designed/raw offline/'
min_id = 0
max_id = 10000



for filename in os.listdir(local_dir):
   print(filename)
   
   df_car = treatCarCSV(filename)  
   df_peds, nbPed, min_id, max_id = treatPedCSV(filename, df_car)
   
   print("Start at "+ str(df_car.time.min()) + "s and end at " + str(df_car.time.max()) + "s");
   
   #add new shifted time column
   df_car["new_time"] =  df_car["time"] - df_car.time.min() 
   df_peds["new_time"] =  df_peds["time"] - df_peds.time.min() 
   
   dataPedById = {}
   for ped_id in range(min_id, max_id):
      dataPedById[ped_id] = df_peds[(df_peds.id == ped_id)]
   
   dataPedByIdFiltered, nbPed, min_id, max_id = filterPedTime(dataPedById)
   dataPedByIdFiltered, nbPed, min_id, max_id = filterPedDistance(dataPedByIdFiltered)
   #Custom filter to adapt to each scenario
   #dataPedByIdFiltered, nbPed, min_id, max_id = filterPedTooFar(dataPedByIdFiltered)
   
   # Write clean data to csv 
   df_car.to_csv(local_dir + filename + "/../../clean_ENU_Map_position.csv", index=False)

   df_p = pd.concat(dataPedByIdFiltered.values(), ignore_index=True)
   df_p.to_csv(local_dir + filename + "/../../clean_tracked_pedestrians_.csv", index=False)            
    
    
    
    
   # Prepare plot to check
   fig, ax = plt.subplots()
   plt.gca().set_xlim([df_car.xGlobal.min() - 5, df_car.xGlobal.max() + 5])
   plt.gca().set_ylim([df_car.yGlobal.min() - 5, df_car.yGlobal.max() +5])
   plt.gca().set_aspect('equal', adjustable='box')

   # Draw car
   width = 4.6
   height = 1.8
   ts = ax.transData

   
   #for i in np.arange(start, nbSec, step):
   for index, row  in df_car.iterrows():
         tr = mplt.transforms.Affine2D().rotate_deg_around(row['xGlobal'], row['yGlobal'], row['heading'] * (180.0 / 3.14))
         t = tr + ts
         rect1 = mplt.patches.Rectangle(xy=(row['xGlobal'] - width / 2, row['yGlobal'] - height / 2),
                                      width=width, height=height, linewidth=1, color='black', fill=False,
                                      alpha=((1.1 * index) / df_car.tail(1).index.item()) / 10.0, 
                                      transform=t)
         ax.add_patch(rect1)
         #plt.arrow(row['xGlobal'], row['yGlobal'],
         #        row['lon_speed'] * m.cos(row['heading']) - row['lat_speed'] * m.sin(row['heading']),
         #        row['lon_speed'] * m.sin(row['heading']) + row['lat_speed'] * m.cos(row['heading']))
 
 
 
   # Draw ped
   cmap = plt.get_cmap('gist_rainbow')
   colors = [cmap(i) for i in np.linspace(0, 1, nbPed+1)]
   i=0
   for p in dataPedByIdFiltered.keys():
         print(p)
         ped = dataPedByIdFiltered[p]
         mark = 'o'
         plt.plot(ped['x'], ped['y'], c=colors[i])#, s=50, marker=mark, edgecolors='black')
         i = i+1
                  
plt.show()
fig.savefig('../'+filename+'_cleaned.png')

   
   
  
  

