import pandas as pd
import os
import numpy as np
import math as m

local_dir = '../filtered offline/'
df_all = pd.DataFrame()
i = -1
# car dimensions
L = 4
W = 2
a = m.sqrt(2) * L / 2
b = m.sqrt(2) * W / 2
j = 0
epsilon = 0.01


def ellipse_closest(semi_major, semi_minor, df_ped):
    res_x = []
    res_y = []
    for t in df_ped.new_time:
        p1 = df_ped.xl[df_ped.new_time == t].values[0]
        p2 = df_ped.yl[df_ped.new_time == t].values[0]
        px = abs(p1)
        py = abs(p2)

        tx = 0.707
        ty = 0.707

        a = semi_major
        b = semi_minor

        for x in range(0, 3):
            x = a * tx
            y = b * ty

            ex = (a * a - b * b) * tx**3 / a
            ey = (b * b - a * a) * ty**3 / b

            rx = x - ex
            ry = y - ey

            qx = px - ex
            qy = py - ey

            r = m.hypot(ry, rx)
            q = m.hypot(qy, qx)

            tx = min(1, max(0, (qx * r / q + ex) / a))
            ty = min(1, max(0, (qy * r / q + ey) / b))
            t = m.hypot(ty, tx)
            tx /= t
            ty /= t
        res_x.append(m.copysign(a * tx, p1))
        res_y.append(m.copysign(b * ty, p2))
    return res_x, res_y


for filename in os.listdir(local_dir):
    i += 1
    print(filename)
    df_car = pd.read_csv(local_dir + filename + "/clean_ENU_Map_position.csv")
    df_car = df_car[(df_car.xGlobal > 0) | (df_car.yGlobal > 0)]
    df_car = df_car.sort_values('new_time')
    df_car = df_car.drop_duplicates(['new_time'])
    df_peds = pd.read_csv(local_dir + filename +
                         "/no_duplicate_clean_tracked_pedestrians_" + ".csv")
    df_peds = df_peds[(df_peds.stamp.isnull())]
    id_list = df_peds.id.unique()
    df_peds['acc_linear'] = 0.0
    df_peds['acc_ang'] = 0.0
    for ped_id in id_list:
        df_ped = df_peds[df_peds.id == ped_id]
        df_ped = df_ped.sort_values('new_time')
        # acceleration
        df_peds.loc[df_peds[df_peds.id == ped_id].index.tolist(), 'acc_linear'] = df_ped['speed'].diff() / df_ped['new_time'].diff()
        df_peds.loc[df_peds[df_peds.id == ped_id].index.tolist(), 'acc_ang'] = df_ped['ori'].diff() / df_ped['new_time'].diff()
    df_peds['closest_x'], df_peds['closest_y'] = ellipse_closest(a, b, df_peds)
    df_peds['dist_veh'] = (np.sqrt(df_peds['xl']**2 + df_peds['yl']**2) - np.sqrt(df_peds['closest_x']**2 + df_peds['closest_y']**2)).tolist()
    df_peds['ori_veh'] = np.arctan2(df_peds['yl'], df_peds['xl']).tolist()
    df_peds['ori_veh'] = df_peds['ori_veh'] * 180 / m.pi

    df_peds.replace(np.inf, 0, inplace=True)
    df_peds.replace(-np.inf, 0, inplace=True)
    #save
    df_peds.to_csv(local_dir + filename + '/no_duplicate_clean_tracked_pedestrians_extended.csv')


df_stat = pd.DataFrame()

for filename in os.listdir(local_dir):
    print(filename)
    df_car = pd.read_csv(local_dir + filename + "/clean_ENU_Map_position.csv")
    df_car = df_car[(df_car.xGlobal > 0) | (df_car.yGlobal > 0)]
    df_peds = pd.read_csv(local_dir + filename +
                         "/no_duplicate_clean_tracked_pedestrians_extended" + ".csv")
    id_list = df_peds.id.unique()

    df_all = pd.DataFrame()
    N = len(df_peds.id.unique())
    df_all['sim_id'] = [0] * N
    df_all['ped_id'] = [0] * N
    df_all['vel_mean'] = [0.0] * N
    df_all['vel_max'] = [0.0] * N
    df_all['vel_min'] = [0.0] * N
    df_all['acc_linear_mean'] = [0.0] * N
    df_all['acc_linear_max'] = [0.0] * N
    df_all['acc_linear_min'] = [0.0] * N
    df_all['acc_ang_mean'] = [0.0] * N
    df_all['acc_ang_max'] = [0.0] * N
    df_all['acc_ang_min'] = [0.0] * N
    df_all['min_dist_veh'] = [0.0] * N
    df_all['max_dist_veh'] = [0.0] * N
    df_all['mean_dist_veh'] = [0.0] * N
    df_all['ped_vel_min_dist'] = [0.0] * N
    df_all['veh_vel_min_dist'] = [0.0] * N
    df_all['ped_accLin_min_dist'] = [0.0] * N
    df_all['veh_accLin_min_dist'] = [0.0] * N
    df_all['ped_accAng_min_dist'] = [0.0] * N
    df_all['veh_accAng_min_dist'] = [0.0] * N

    i = 0
    for ped_id in id_list:
        df_all.loc[i, 'sim_id'] = j
        df_all.loc[i, 'ped_id'] = ped_id
        df_ped = df_peds[df_peds.id == ped_id]
        # speed
        df_all.loc[i, 'vel_mean'] = df_ped.speed.mean()
        df_all.loc[i, 'vel_max'] = df_ped.speed.max()
        df_all.loc[i, 'vel_min'] = df_ped.speed.min()
        # acceleration
        df_all.loc[i, 'acc_linear_mean'] = df_ped.acc_linear.mean()
        df_all.loc[i, 'acc_linear_max'] = df_ped.acc_linear.abs().max()
        df_all.loc[i, 'acc_linear_min'] = df_ped.acc_linear.abs().min()
        df_all.loc[i, 'acc_ang_mean'] = df_ped.acc_ang.abs().mean()
        df_all.loc[i, 'acc_ang_max'] = df_ped.acc_ang.abs().max()
        df_all.loc[i, 'acc_ang_min'] = df_ped.acc_ang.abs().min()
        # dist veh
        df_all.loc[i, 'min_dist_veh'] = df_ped.dist_veh.min()
        df_all.loc[i, 'max_dist_veh'] = df_ped.dist_veh.max()
        df_all.loc[i, 'mean_dist_veh'] = df_ped.dist_veh.mean()
        # vel at min dist: Ped
        df_all.loc[i, 'ped_vel_min_dist'] = df_ped.speed[df_ped.dist_veh == df_ped.dist_veh.min()].mean()
        min_dist_time = df_ped.new_time[df_ped.dist_veh == df_ped.dist_veh.min()]
        df_all.loc[i, 'ped_accLin_min_dist'] = df_ped.acc_linear[df_ped.dist_veh == df_ped.dist_veh.min()].mean()
        df_all.loc[i, 'ped_accAng_min_dist'] = df_ped.acc_ang[df_ped.dist_veh == df_ped.dist_veh.min()].mean()
        # vel at min dist: Veh
        df_all.loc[i, 'veh_vel_min_dist'] = df_car.lon_speed[abs(df_car.new_time - min_dist_time) <= epsilon].mean()

        i += 1
    if j == 0:
        df_stat = df_all
    else:
        df_stat = df_stat.append(df_all)
    j += 1

df_stat.to_csv('../statistics.csv')

